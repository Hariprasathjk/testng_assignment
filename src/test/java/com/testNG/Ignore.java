package com.testNG;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Ignore {      
	@BeforeSuite
	private void Setproperety() {
	System.out.println("Set property");
}
@BeforeTest
private void Browserlaunch() {
	System.out.println("browserlaunch");
}
@BeforeClass(enabled = false)
private void url() {
	System.out.println("url");
}
@BeforeMethod
private void login() {
	System.out.println("login");
}

@Test(dependsOnMethods ="earphone",groups = "Electronics")
private void laptopSearch() {
	System.out.println("laptop Search");
	}
@Test(priority = -2,invocationCount = 2,groups = "Electronics")
private void mobilesearch() {
	System.out.println("Mobile Search");
	
	}
@Test(priority = 0,groups = "Electronics")
private void earphone() {
	System.out.println("ear phone");
	}
@Test
private void book() {
	System.out.println("book");

}

@AfterMethod
private void logout() {
	System.out.println("log out");
}
@AfterClass
private void homepage() {
	System.out.println("homepage");
	

}

@AfterTest
private void close() {
	System.out.println("close");
}
@AfterSuite

private void deleteallcookies() {
	System.out.println("delete all cookies");
	
}


}
