package com.testNG;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Parameterized_Test {
	@Test
	@Parameters({"username","password"})
	private void credentials(@Optional("hari")String username,int password) {
		System.out.println("username:  "+username);
		System.out.println("password:  "+password);
		

	}

}
